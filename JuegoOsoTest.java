import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
/**
 * The test class JuegoOsoTest.
 *
 * @author Jorge Loarte && Kelvin J. Carreño
 * @version 1.0
 */
public class JuegoOsoTest{
    
    JuegoOso p;
    /**
     * Default constructor for test class JuegoOsoTest
     */
    public JuegoOsoTest(){
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp(){
    } 

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    //Mira si al introducir la fila es correcto o no
    @Test
    public void LimiteFilaMenor0(){
         p = new JuegoOso(-1, 6, 's');
         assertEquals("Fila es menor a 0",p.compruebaFila());
    }
    @Test
    public void LimiteFilaMayor7(){
         p = new JuegoOso(10, 6, 's');
         assertEquals("Fila es mayor a 7",p.compruebaFila());
    }
    @Test
    public void LimiteFilaEnElTablero(){
         p = new JuegoOso(5, 6, 's');
         assertEquals("Fila entra en el tablero",p.compruebaFila());
    }
    @Test
    public void LimiteFilaValorMaxímo(){
         p = new JuegoOso(Integer.MAX_VALUE, 6, 's');
         assertEquals("Fila es mayor a 7",p.compruebaFila());
    }
    @Test
    public void LimiteFilaValorMínimo(){
         p = new JuegoOso(Integer.MIN_VALUE, 6, 's');
         assertEquals("Fila es menor a 0",p.compruebaFila());
    }
    @Test
    public void LimiteFilaLetra(){
         p = new JuegoOso('i', 6, 's');
         assertEquals("Fila es mayor a 7",p.compruebaFila());
    }
    
    //Mira si al introducir la columna es correcto o no
    @Test
    public void LimiteColumnaMenor0(){
         p = new JuegoOso(5, -12, 's');
         assertEquals("Columna es menor a 0",p.compruebaColumna());
    }
    @Test
    public void LimiteColumnaMayor7(){
         p = new JuegoOso(5, 19, 's');
         assertEquals("Columna es mayor a 7",p.compruebaColumna());
    }
    @Test
    public void LimiteColumnaEnElTablero(){
         p = new JuegoOso(2, 4, 's');
         assertEquals("Columna entra en el tablero",p.compruebaColumna());
    }
    @Test
    public void LimiteColumnaValorMaxímo(){
         p = new JuegoOso(7, Integer.MAX_VALUE, 's');
         assertEquals("Columna es mayor a 7",p.compruebaColumna());
    }
    @Test
    public void LimiteColumnaValorMínimo(){
         p = new JuegoOso(6, Integer.MIN_VALUE, 's');
         assertEquals("Columna es menor a 0",p.compruebaColumna());
    }
    @Test
    public void LimiteColumnaLetra(){
         p = new JuegoOso(6, 'o', 's');
         assertEquals("Columna es mayor a 7",p.compruebaColumna());
    }
    
    ////Mira si al introducir la letra es correcto o no
    @Test
    public void LetraEsS(){
         p = new JuegoOso(2, 4, 's');
         assertEquals("Letra es la S",p.compruebaLetra());
    }
    @Test
    public void LetraEsO(){
         p = new JuegoOso(2, 4, 'O');
         assertEquals("Letra es la O",p.compruebaLetra());
    }
    @Test
    public void NoEsSniO(){
         p = new JuegoOso(2, 4, 'l');
         assertEquals("La letra distinta a O o a S",p.compruebaLetra());
    }
    
    //Comprueba si se ha conseguido todos los puntos
    @Test
    public void ComprobacionPuntos1(){
         assertTrue(p.ComprobacionFinal(20,14));
    }
    @Test
    public void ComprobacionPuntos2(){
         assertTrue(p.ComprobacionFinal(15,21));
    }
    @Test
    public void ComprobacionPuntos3(){
         assertTrue(!p.ComprobacionFinal(12,14));
    }
    
    //Comprueba quien es el ganador
    @Test
    public void Ganador1(){
         assertEquals("ganadoJ1",p.compruebaPuntos(20,5));
    }
    @Test
    public void Ganador2(){
         assertEquals("ganadoJ2",p.compruebaPuntos(7,19));
    }
    @Test
    public void Empate(){
         assertEquals("empate",p.compruebaPuntos(7,7));
    }
}
