import java.util.Scanner;
/**
 * Write a description of class JuegoOso here.
 * 
 * @author Kelvin J. Carreño && Jorge Loarte 
 * @version 1.0
 */
public class JuegoOso {
    /*inicializacion de las variables publicas*/
    static Scanner sc = new Scanner(System.in);
    static int DIM = 8;
    static int fila;
    static int columna;
    static char O = 'O';
    static char S = 'S';
    static char N = ' ';
    static char letra;
    static char[][] Tablero = new char[DIM][DIM];
    static int turno = 0;
    static boolean end;
    static int jugador = 0;
    static int puntos[] = {0, 0};
    static boolean cambioTurno;
    static int TotalPoints = 20;
    
    public JuegoOso(int a, int b, char letter){
        fila = a;
        columna = b;
        letra = letter;
    }
    
    //Dibuja tablero
    public static void tablero() {
       clearScreen();
       dibujaTitulo();
       //Dibujar Tablero
       System.out.print("    ");
       for(int i = 0; i<DIM; i++)
           System.out.print(" " + i + "  ");
       System.out.println();
       System.out.print("    ");
       for(int i = 0; i<DIM; i++)
           System.out.print("----");
       System.out.println();
       for(int f = 0; f<DIM; f++){
           System.out.print(f + "   ");
           System.out.print("|");
           for(int c = 0; c<DIM; c++)
               System.out.print(" " + Tablero[f][c] + " \u23D0");
           System.out.println();
           System.out.print("    ");
           for(int i = 0; i<DIM; i++)
               System.out.print("----");
           System.out.println();
       }
       System.out.println();
    }
    
    //Limpia la pantalla
    public static void clearScreen(){
       System.out.print("\u000C");
    }
    
    public static void dibujaTitulo(){
       System.out.println("JUEGO DEL OSO");
       System.out.println("Puntos:");
       System.out.print(" Jugador 1: " + puntos[0]);
       System.out.println("  Jugador 2: " + puntos[1]);
       System.out.println();
    }
    
    //Lee la Fila
    public static int ScanFila(){
       String FilaString;
       boolean filaCorrecto, fin;
       do{
           do{
               System.out.print("Dime la fila (0 - 7): ");
               FilaString = sc.next();
               filaCorrecto = FilaString.matches("\\d+");
               if(!filaCorrecto)
                    System.out.println("¡Cuidado! Solo puedes insertar números.\n");
           }while(!filaCorrecto);
           fila = Integer.parseInt(FilaString);
           if(compruebaFila() == "Fila entra en el tablero")
                fin = true;
           else{
                fin = false;
                System.out.println("¡Cuidado! Ese número no es válido\n");
           }
       }while(!fin);
       return fila;
    }
   
    //Lee la Columna
    public static int ScanColumna(){
        String ColumnaString;
        boolean columCorrecto, fin;
        do{
            do{
                System.out.print("Dime la columna (0 - 7): ");
                ColumnaString = sc.next();
                columCorrecto = ColumnaString.matches("\\d+");
                if(!columCorrecto)
                    System.out.println("¡Cuidado! Solo puedes insertar números.\n");
            }while(!columCorrecto);
            columna = Integer.parseInt(ColumnaString);
            if(compruebaColumna() == "Columna entra en el tablero")
                fin = true;
            else{
                fin = false;
                System.out.println("¡Cuidado! Ese número no es válido\n");
            }
        }while(!fin);
        return columna;
    }
    
    //Pregunta que fila y comluna quieres 
    public static boolean dimeDonde() {
       ScanColumna();
       ScanFila();
       
       if(Tablero[fila][columna] != N){
           System.out.println("Ese espacio está ocupado");
           dimeDonde();
           return false;
       }
       else{
           dimeLetra();
           return true;
       }
    }
    
    //Comprueba la fila insertada
    public static String compruebaFila(){
       if(fila < 0)
            return "Fila es menor a 0";
       else if(fila > 7)
            return "Fila es mayor a 7";
       else if(fila >= 0 && fila <= 7 )
            return "Fila entra en el tablero";
       else
            return "FAIL";
    }
    
    //Comprueba la columna insertada
    public static String compruebaColumna(){
       if(columna < 0)
            return "Columna es menor a 0";
       else if(columna > 7)
            return "Columna es mayor a 7";
       else if(columna >= 0 && columna <= 7 )
            return "Columna entra en el tablero";
       else
            return "FAIL";
    }
    
    //Pregunta la letra quieres 
    public static void dimeLetra(){
       System.out.print("¿Que letra quieres introducir? ('S'-'O'): ");
       letra = sc.next().charAt(0);
       
       if(compruebaLetra() == "Letra es la S")
            letra = S;
       else if(compruebaLetra() == "Letra es la O")
            letra = O;
       else {
           System.out.println("Solo puedes introducir la letra 'S' o la letra 'O'");
           dimeLetra();
       }
    }
    
    //Comprueba la letra insertada
    public static String compruebaLetra(){
       if(letra == 's' || letra == 'S')
            return "Letra es la S";
       else if(letra == 'o' || letra == 'O')
            return "Letra es la O";
       else
            return "La letra distinta a O o a S";
    }
    
    public static void turnos() {
       jugador = turno % 2;
       System.out.println("Le toca al Jugador " + (jugador+1));
       System.out.println();
    }

    public static void compruebaPalabra(){
       //Inicializamos cambioTurno para que sea true inicialmente 
       cambioTurno = true;
       //Comprovación cuando pones una S
       if(fila > 0 && fila < 7){ //Vertical
           if(Tablero[fila][columna] == S &&
            Tablero[fila-1][columna] == O &&
            Tablero[fila+1][columna] == O){
                cambioTurno = false;
                puntos[jugador%2]++;
            }
       }
       if(columna > 0 && columna < 7){ //Horizontal
           if(Tablero[fila][columna] == S &&
            Tablero[fila][columna-1] == O &&
            Tablero[fila][columna+1] == O){
                cambioTurno = false;
                puntos[jugador%2]++;
           }
       }
       //Comprovación cuando pones una O
       if(fila <= 4){//Hacia Abajo
           if(Tablero[fila][columna] == O &&
            Tablero[fila+1][columna] == S &&
            Tablero[fila+2][columna] == O){
                cambioTurno = false;
                puntos[jugador%2]++;
           }
       }
       if(fila >= 2){//Hacia Arriba
           if(Tablero[fila][columna] == O &&
            Tablero[fila-1][columna] == S &&
            Tablero[fila-2][columna] == O){
                cambioTurno = false;
                puntos[jugador%2]++;
           }
       }
       if(columna <= 4){//Hacia la Derecha
           if(Tablero[fila][columna] == O &&
            Tablero[fila][columna+1] == S &&
            Tablero[fila][columna+2] == O){
                cambioTurno = false;
                puntos[jugador%2]++;
           }
       }
       if(columna >= 2){//Hacia la Izquierda
           if(Tablero[fila][columna] == O &&
            Tablero[fila][columna-1] == S &&
            Tablero[fila][columna-2] == O){
                cambioTurno = false;
                puntos[jugador%2]++;
           }
       }
       //Cambia de palabra dependiendo si está OSO en el tablero o no 
       if(cambioTurno == true){
           turno++;
       }
    }
    
    //Comprueba los puntos de Jugador1 y Jugador2 y devuelve quién ha ganado 
    public static String compruebaPuntos(int punt1, int punt2){
       if(punt1 > punt2){
           System.out.println("Ha ganado el jugador 1");    
           return "ganadoJ1";
       }
       else if(punt2 > punt1){
           System.out.println("Ha ganado el jugador 2");
           return "ganadoJ2";
       }
       else{
           System.out.println("Han quedado empate");
           return "empate";
       }
    }
    
    //Comprueba si se han consegido el límite de Puntos
    public static boolean ComprobacionFinal(int punt1, int punt2){
       if(punt1 >= TotalPoints || punt2 >= TotalPoints)
            return true;
       return false;
    }
    
    public static void main() {
       //Pone todo el tablero en N == ' '
       for(int i=0; i<DIM; i++)
           for(int j=0; j<DIM; j++)
               Tablero[i][j] = N;
       
       //Bucle del juego se para al límite de puntos obtenidos
       do{
            tablero();
            turnos();
            dimeDonde();      
            Tablero[fila][columna] = letra;
            compruebaPalabra();
            end = ComprobacionFinal(puntos[0], puntos[1]);
       }while(!end);
       
       tablero();
       compruebaPuntos(puntos[0],puntos[1]);
       
       //Cierra el Scanner
       sc.close();
    }
}

